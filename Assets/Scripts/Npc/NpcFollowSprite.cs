using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NpcFollowSprite : MonoBehaviour
{
    public Transform player;
    [SerializeField] private NpcBehavior _npcBehavior;
    [SerializeField] private float triggerDistance;
    [SerializeField] private float triggerHeightDistance;
    // Update is called once per frame
    void Update()
    {
        float distance = Vector2.Distance(player.position, transform.position);
        float heightDistance = Mathf.Abs(player.position.y - transform.position.y);
        if (Mathf.Abs(distance) < triggerDistance && heightDistance < triggerHeightDistance)
        {
            if (player.position.x < transform.position.x)
            {
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }
            else
            {
                transform.rotation = Quaternion.Euler(0,180, 0);
            }
        }
        else if (!_npcBehavior.IsTalking())
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
    }
}
