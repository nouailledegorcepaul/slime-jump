using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

public enum NpcState
{
    IDLE, DELIVERY, ASK, CLAIM
}

public class Npc : MonoBehaviour
{
    public int id;
    public List<Convo> convos;
    public NpcState state;
    [HideInInspector] public bool firstEncounter;
    [HideInInspector] public NpcBubble bubble;
    public List<QuestGoal> deliveryQuestGoal;
    public List<Quest> questsToGive;
    public List<Quest> questsToClaim;
    public List<Quest> questsComplete;

    private void Awake()
    {
        firstEncounter = true;
        NpcManager.Npcs[id] = this;
        bubble = GetComponentInChildren<NpcBubble>();
        deliveryQuestGoal = new List<QuestGoal>();
        questsToGive = new List<Quest>();
        questsToClaim = new List<Quest>();
        questsComplete = new List<Quest>();
        SetNextConvo();
    }

    private void Update()
    {
        if (firstEncounter || questsToGive.Count + questsToClaim.Count + deliveryQuestGoal.Count == 0)
        {
            state = NpcState.IDLE;
            return;
        }
        if (deliveryQuestGoal.Count > 0)
        {
            state = NpcState.DELIVERY;
            return;
        }
        
        if (questsToClaim.Count > 0)
        {
            state = NpcState.CLAIM;
            return;
        }

        if (questsToGive.Count > 0)
        {
            state = NpcState.ASK;
        }
    }

    public void AddQuestToGive(Quest q)
    {
        q.status = QuestStatus.Waiting;
        questsToGive.Add(q);
    }
    
    public void AddQuestToClaim(Quest q)
    {
        q.status = QuestStatus.Pending;
        questsToClaim.Add(q);
    }

    public void AddQuestToComplete(Quest q)
    {
        q.status = QuestStatus.Complete;
        questsComplete.Add(q);
        QuestUpdater.UpdateQuests();
        SoundManager.Instance.PlayQuestCompleteSound();
        if (q.lastQuest && !EndScreen.Instance.content.activeSelf)
        {
            SoundManager.Instance.PlayEndGameSound();
            EndScreen.Instance.OpenWindow();
        }
    }

    public void AddDeliveryQuestGoal(QuestGoal goal)
    {
        deliveryQuestGoal.Add(goal);
    }
    
    public void SetNextConvo()
    {
        if (!bubble.IsBusy())
        {
            if (firstEncounter)
            {
                foreach (Convo convo in convos)
                {
                    if (convo.type == DialogueType.Greetings)
                    {
                        bubble.SetConvo(convo);
                        convos.Remove(convo);
                        return;
                    }
                } 
            }
            // If it exists one quest to claim, we set the conv and remove the quest from the npc list.
            if (state == NpcState.CLAIM)
            {
                Quest claim = questsToClaim[0];
                bubble.SetConvo(claim);
                questsToClaim.Remove(claim);
                return;
            }
            // If it exists one new quest to give, we set the conv and remove the quest from the npc list.
            if (state == NpcState.ASK)
            {
                Quest quest = questsToGive[0];
                bubble.SetConvo(quest);
                questsToGive.Remove(quest);
                return;
            }

            if (state == NpcState.DELIVERY)
            {
                QuestGoal goal = deliveryQuestGoal[0];
                bubble.SetConvo(goal);
                deliveryQuestGoal.Remove(goal);
                return;
            }
            // Nothing to do with quests, we set a random common conv
            bubble.SetConvo(convos[Random.Range(0, convos.Count)]);  
        }
    }
}