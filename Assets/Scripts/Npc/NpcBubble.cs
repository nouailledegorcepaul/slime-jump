using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class NpcBubble : MonoBehaviour
{
    private Npc _npc;
    public TextMeshPro text;
    public GameObject bubble;
    private NpcBehavior _npcBehavior;
    private Convo _currentDialogue;
    private Quest _quest;
    private QuestGoal _goal;
    private float _currentTime;
    // private int _index;
    // Start is called before the first frame update
    void Start()
    {
        _npcBehavior = GetComponentInParent<NpcBehavior>();
        _npc = GetComponentInParent<Npc>();
        bubble.SetActive(false);
    }
    

    public IEnumerator StartDialogueCo()
    {
        bubble.SetActive(true);
        foreach (string sentence in _currentDialogue.sentences)
        {
            text.text = sentence;
            yield return new WaitForSeconds(4f);
        }
        // Refresh after greetings to correctly update exclamation mark if new quests
        _npcBehavior.EndTalk(_quest, _goal);
        if (_currentDialogue.type == DialogueType.Greetings)
        {
            _npc.firstEncounter = false;
        }
        _currentDialogue = null;
        bubble.SetActive(false);
    }
    
    public bool IsBusy()
    {
        return !_currentDialogue.IsUnityNull();
    }
    
    public void SetConvo(Convo convo)
    {
        _quest = null;
        _goal = null;
        _currentDialogue = convo;
        text.text = _currentDialogue.sentences[0];
    }

    public void SetConvo(Quest quest)
    {
        _quest = quest;
        _goal = null;
        if (_quest.status == QuestStatus.Pending)
        {
            _currentDialogue = quest.endQuestConvo;
        }
        else
        {
            _currentDialogue = quest.startQuestConvo;
        }
        text.text = _currentDialogue.sentences[0];
    }

    public void SetConvo(QuestGoal delivery)
    {
        _quest = QuestInventory.Instance.Quests[delivery.questId];
        _goal = delivery;
        _currentDialogue = _goal.destinationConvo;
        text.text = _currentDialogue.sentences[0];
    }
}