using System.Linq;
using TMPro;
using UnityEngine;

public class NpcBehavior : MonoBehaviour
{
    private Npc _npc;
    public Color mainColor, secondaryColor;
    public GameObject exclamationMark;
    public GameObject questionMark;
    public GameObject cloudIcon;
    public GameObject interact;
    private bool _playerInRange;
    private bool _npcTalking;
    // Start is called before the first frame update
    void Start()
    {
        _npc = GetComponent<Npc>();
        interact.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        exclamationMark.SetActive(_npc.state == NpcState.ASK);
        questionMark.SetActive(_npc.state == NpcState.CLAIM);
        cloudIcon.SetActive(_npc.state == NpcState.DELIVERY);
        if (_npc.state == NpcState.ASK)
        {
            if (_npc.questsToGive[0].mainQuest)
            {
                exclamationMark.GetComponent<TextMeshPro>().color = mainColor;
            }
            else
            {
                exclamationMark.GetComponent<TextMeshPro>().color = secondaryColor;
            }
        }
        if (_npc.state == NpcState.CLAIM)
        {
            if (_npc.questsToClaim[0].mainQuest)
            {
                questionMark.GetComponent<TextMeshPro>().color = mainColor;
            }
            else
            {
                questionMark.GetComponent<TextMeshPro>().color = secondaryColor;
            }
        }
        if (_playerInRange && !_npcTalking)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                GetComponent<Npc>().SetNextConvo();
                StartCoroutine(GetComponent<NpcBubble>().StartDialogueCo());
                interact.SetActive(false);
                _npcTalking = true;
            }
        }
    }

    public void EndTalk(Quest quest=null, QuestGoal goal=null)
    {
        if (quest is not null)
        {
            if (goal is not null)
            {
                goal.Delivered();
            }
            else if (quest.status == QuestStatus.Waiting)
            {
                quest.status = QuestStatus.Started;
                // Add quest to quests list
                QuestInventory.Instance.Quests[quest.id] = quest;
                print("QUEST ADDED TO QUEST TAB");
                
                // Instantiate a quest card to show on the quest tab
                GameObject questDisplay = Instantiate(Resources.Load("Quest Card"), QuestInventory.Instance.questContainer) as GameObject;
                questDisplay.GetComponent<QuestCard>().InitCard(quest);
                QuestUpdater.UpdateQuest(quest.id);
                SoundManager.Instance.PlayQuestAddedSound();
            }
            else if (quest.status == QuestStatus.Pending)
            {
                quest.goals.Last().type = QuestType.Delivery;
                quest.goals.Last().delivered = true;
                _npc.AddQuestToComplete(quest);
            }
        }
        _npcTalking = false;
        exclamationMark.SetActive(false);
        questionMark.SetActive(false);
    }

    public bool IsTalking()
    {
        return _npcTalking;
    }
    
    private void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            interact.SetActive(!_npcTalking);
            _playerInRange = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            interact.SetActive(false);
            _playerInRange = false;
        }
    }
}
