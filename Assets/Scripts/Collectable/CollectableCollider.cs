using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class CollectableCollider : MonoBehaviour
{
    public QuestGoal questGoal;
    
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            if (gameObject.CompareTag("monster"))
            {
                if (!col.gameObject.GetComponent<MonsterKillBuff>().IsUnityNull())
                {
                    questGoal.AddCurrentAmount();
                    Destroy(GetComponent<CollectableCollider>());
                }
            }
            else
            {
                questGoal.AddCurrentAmount();
                if (gameObject.CompareTag("infos"))
                {
                    Destroy(GetComponent<SpriteRenderer>());
                    GetComponent<InfosCameraBehavior>().Init();
                    return;
                }

                if (gameObject.CompareTag("carrot"))
                {
                    SoundManager.Instance.PlayGoldenCarrot();
                }
                SoundManager.Instance.PlayCollectItem();
                Destroy(gameObject);
            }
        }
    }
}
