using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class CollectableInit : MonoBehaviour
{
    public QuestGoalData questGoalData;
    
    private void Update()
    {
        if (!QuestInventory.Instance.Quests.IsUnityNull())
        {
            if (QuestInventory.Instance.initDone)
            {
                Quest q = QuestInventory.Instance.Quests[questGoalData.questId];
                QuestGoal questGoal = q.goals.Find(goal => goal.id == questGoalData.id);
                gameObject.AddComponent<CollectableCollider>();
                GetComponent<CollectableCollider>().questGoal = questGoal;
                Destroy(this);
            }
        }
    }
}
