using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class MonsterHitBox : MonoBehaviour
{
    private CameraBehavior cam;

    private void Start()
    {
        cam = Camera.main.GetComponent<CameraBehavior>();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            if (!col.GetComponent<MonsterKillBuff>().IsUnityNull())
            {
                cam.fix = true;
                GetComponent<MonsterKilledBehavior>().enabled = true;
                Destroy(col.GetComponent<MonsterKillBuff>());
                SoundManager.Instance.PlayHitSound();
            }
            else
            {
                col.gameObject.transform.position = PlayerCheckpoint.currentCheckpoint;
                cam.SetCheckpointLimits();
            }
        }
    }
}
