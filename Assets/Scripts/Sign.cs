using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Sign : MonoBehaviour
{
    [field: TextArea]
    public string infos;
    public GameObject bubble;
    public TextMeshPro text;
    // Start is called before the first frame update
    void Start()
    {
        text.text = infos;
        bubble.SetActive(false);
    }

    private void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            if (PlayerState.state == State.Move)
            {
                bubble.SetActive(true);
            }
            else
            {
                bubble.SetActive(false);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            bubble.SetActive(false);
        }
    }
}
