using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class DeathTrigger : MonoBehaviour
{
    public bool isMonsterZone;
    
    
    private void OnTriggerEnter2D(Collider2D col)
    {
        
        if (col.gameObject.CompareTag("Player"))
        {
            if (isMonsterZone)
            {
                if (col.gameObject.GetComponent<MonsterKillBuff>().IsUnityNull())
                {
                    GameOver(col.gameObject);
                }
                else
                {
                    Destroy(gameObject);
                }
            }
            else
            {
                GameOver(col.gameObject);
            }
        }
    }

    private void GameOver(GameObject go)
    {
        go.transform.position = PlayerCheckpoint.currentCheckpoint;
        Camera.main.gameObject.GetComponent<CameraBehavior>().SetCheckpointLimits();  
    }
}
