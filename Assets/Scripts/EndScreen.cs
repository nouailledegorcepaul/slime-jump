using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class EndScreen : MonoBehaviour
{
    public GameObject content;
    public static EndScreen Instance;
    public TextMeshProUGUI completeGameText;
    public string uncompleteText;
    public string completeText;
    public bool complete;

    private void Awake()
    {
        if (Instance is null)
        {
            Instance = this;
        }
        else
        {
            print("Existing Instance of EndScreen");
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenWindow()
    {
        content.SetActive(true);
        DisplayCompleteGameText();
    }
    
    public void DisplayCompleteGameText()
    {
        if (complete){
            completeGameText.text = completeText;
        }
        else
        {
            completeGameText.text = uncompleteText;
        }
    }
    
    public void CloseWindow()
    {
        content.SetActive(false);
    }
}
