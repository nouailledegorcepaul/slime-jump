using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class CameraChanger : MonoBehaviour
{
    public GameObject cam;

    public Vector2 rightLimit;
    public Vector2 leftLimit;
    public AudioClip musicChange;
    
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            cam.GetComponent<CameraBehavior>().rightLimit = rightLimit;
            cam.GetComponent<CameraBehavior>().leftLimit = leftLimit;
            if (musicChange)
            {
                SoundManager.Instance.PlayMusic(musicChange);
                cam.GetComponent<CameraBehavior>().musicChange = musicChange;
            }
        }
    }
}
