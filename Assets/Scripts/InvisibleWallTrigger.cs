using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Tilemaps;

public class InvisibleWallTrigger : MonoBehaviour
{
    public Tilemap tilemap;
    private BoundsInt _bounds;
    private Color _c;
    private bool _playerIn;
    void Start()
    {
        _bounds = tilemap.cellBounds;
        _c = new Color(1, 1, 1, 1f);
    }

    private void Update()
    {
        if (_playerIn)
        {
            if (_c.a > 0.2f)
            {
                tilemap.color = _c;
                _c.a -= 0.01f;  
            }
        }
        else
        {
            if (_c.a < 1f)
            {
                tilemap.color = _c;
                _c.a += 0.01f;  
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            _playerIn = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _playerIn = false;
        }
    }
}
