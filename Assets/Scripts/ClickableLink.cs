using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClickableLink : MonoBehaviour, IPointerClickHandler
{
    private TMP_Text _text;

    public delegate void ClickOnLinkEvent(string keyword);

    public static event ClickOnLinkEvent OnClickedOnLinkEvent;
    // Start is called before the first frame update
    void Start()
    {
        _text = GetComponent<TextMeshProUGUI>();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        Vector3 mousePosition = new Vector3(eventData.position.x, eventData.position.y, 0);
        int linkIndex = TMP_TextUtilities.FindIntersectingLink(_text, mousePosition, eventData.pressEventCamera);
        if (linkIndex == -1)
        {
            return;
        }

        TMP_LinkInfo linkInfo = _text.textInfo.linkInfo[linkIndex];
        string linkID = linkInfo.GetLinkID();
        if (linkID.Contains("www") || linkID.Contains("http"))
        {
            Application.OpenURL(linkID);
            return;
        }
        OnClickedOnLinkEvent?.Invoke(linkInfo.GetLinkText());
    }
}
