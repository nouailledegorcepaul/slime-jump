using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfosCameraBehavior : MonoBehaviour
{
    public Camera cam;
    public Vector3 destination;
    public float speed;
    public bool start;

    // Update is called once per frame
    void Update()
    {
        if (start)
        {
            cam.transform.position = Vector3.Lerp(cam.transform.position, destination, speed);
            if (Vector3.Distance(cam.transform.position, destination) < 0.1f)
            {
                cam.GetComponent<CameraBehavior>().track = true;
                PlayerState.state = State.Move;
                Destroy(gameObject);
            }
        }
    }

    public void Init()
    {
        start = true;
        cam.GetComponent<CameraBehavior>().track = false;
        PlayerState.state = State.Stand;
        SoundManager.Instance.PlayMonsterDiscover();
    }
}
