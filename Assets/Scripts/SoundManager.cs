using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
    public AudioSource music, music2, sound;
    public Slider musicSlider, soundSlider;
    public AudioClip jump;
    public AudioClip wallJump;
    public AudioClip goldenCarrot;
    public AudioClip monsterDiscover;
    public AudioClip forestMusic;
    public AudioClip deepForestMusic;
    public AudioClip skyMusic;
    public AudioClip mainMenuMusic;
    public AudioClip gameOverSound;
    public AudioClip collectItemSound;
    public AudioClip hitSound;
    public AudioClip endGameSound;
    public AudioClip questAddedSound;
    public AudioClip questCompleteSound;
    public float transitionTime;
    private AudioSource current, other;
    public static SoundManager Instance;

    private void Awake()
    {
        if (Instance.IsUnityNull())
        {
            musicSlider.value = 0.2f;
            soundSlider.value = 1f;
            current = music;
            other = music2;
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        PlayMainMenu();
    }

    public void PlaySound(AudioClip originalClip)
    {
        sound.PlayOneShot(originalClip, sound.volume);
    }

    public void PlayGoldenCarrot()
    {
        PlayMusic(goldenCarrot);
    }
    
    public void PlayJump(){
        PlaySound(jump);
    }
    
    public void PlayWallJump(){
        PlaySound(wallJump);
    }

    public void PlayMonsterDiscover()
    {
        PlayMusic(monsterDiscover);
    }

    public void PlayMainMenu()
    {
        PlayMusic(mainMenuMusic);
    }
    
    public void PlayForest()
    {
        PlayMusic(forestMusic);
    }
    
    public void PlayDeepForest()
    {
        PlayMusic(deepForestMusic);
    }
    
    public void PlaySky()
    {
        PlayMusic(skyMusic);
    }

    public void PlayGameOver()
    {
        PlaySound(gameOverSound);
    }

    public void PlayCollectItem()
    {
        PlaySound(collectItemSound);
    }
    
    public void PlayHitSound()
    {
        PlaySound(hitSound);
    }
    
    public void PlayEndGameSound()
    {
        PlaySound(endGameSound);
    }
    
    public void PlayQuestAddedSound()
    {
        PlaySound(questAddedSound);
    }
    
    public void PlayQuestCompleteSound()
    {
        PlaySound(questCompleteSound);
    }

    public void PlayMusic(AudioClip ac)
    {
        if (ac != current.clip)
        {
            other.clip = ac;
            other.loop = true;
            StopCoroutine(MusicTransition());
            StopCoroutine(PlayNextMusic());
            StartCoroutine(MusicTransition());  
        }
    }

    public IEnumerator MusicTransition()
    {
        float percentage = 0;
        while (current.volume > 0)
        {
            current.volume = Mathf.Lerp(musicSlider.value, 0, percentage);
            percentage += Time.deltaTime / transitionTime;
            yield return null;
        }
        
        current.Pause();
        if (!other.isPlaying)
        {
            other.Play();
        }
        other.UnPause();
        percentage = 0;
        if (other.clip == goldenCarrot || other.clip == monsterDiscover)
        {
            current.loop = false;
            StartCoroutine(PlayNextMusic());
        }
        
        while (current.volume < musicSlider.value)
        {
            current.volume = Mathf.Lerp(0, musicSlider.value, percentage);
            percentage += Time.deltaTime / transitionTime;
            yield return null;
        }
        (current, other) = (other, current);
    }

    public void StopMusic()
    {
        music.loop = false;
        music.Stop();
    }
    
    public void SetMusicVolume()
    {
        music.volume = musicSlider.value;
        music2.volume = musicSlider.value;
    }
    
    public void SetSoundVolume()
    {
        sound.volume = soundSlider.value;
    }
    
    public IEnumerator PlayNextMusic()
    {
        print(other.clip.length);
        yield return new WaitForSeconds(other.clip.length - 0.5f);
        print("Passe next: " + current.clip.name);
        PlayMusic(other.clip);
        // other.UnPause();
        // (current, other) = (other, current);
    }
}