using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class CameraBehavior : MonoBehaviour
{
    public Vector2 leftLimit, rightLimit;
    public Vector2 leftLimitCheckpoint, rightLimitCheckpoint;
    public AudioClip musicChange;
    public float smoothing;
    public Transform target;
    public bool track;
    public bool fix;
    // Start is called before the first frame update
    void Start()
    {
        track = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!target.IsUnityNull() && track)
        {
            Vector3 targetPos = new Vector3(target.position.x, target.position.y, transform.position.z);
            if (!fix)
            {
                targetPos.x = Mathf.Clamp(targetPos.x, leftLimit.x, rightLimit.x);
                targetPos.y = Mathf.Clamp(targetPos.y, leftLimit.y, rightLimit.y);

                transform.position = Vector3.Lerp(transform.position, targetPos, smoothing); 
            }
            else
            {
                transform.position = Vector3.Lerp(transform.position, targetPos, 0.1f); 
            }
        }
    }

    public void SetCheckpointLimits()
    {
        leftLimit = leftLimitCheckpoint;
        rightLimit = rightLimitCheckpoint;
        SoundManager.Instance.PlayMusic(musicChange);
        SoundManager.Instance.PlayGameOver();
    }
}
