using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainScreen : MonoBehaviour
{
    public GameObject mainScreen;
    public GameObject creditsScreen;
    public GameObject settingsScreen;
    public GameObject settingsExitButton;
    public GameObject questTab;

    private bool gameStarted;
    // Start is called before the first frame update
    void Start()
    {
        questTab.SetActive(false);
        PlayerState.state = State.Stand;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gameStarted)
            {
                if (PlayerState.state == State.Move)
                {
                    OpenSettings();
                }
                else
                {
                    CloseSettings();
                }  
            }
        }
    }
    
    public void StartGame()
    {
        mainScreen.SetActive(false);
        questTab.SetActive(true);
        PlayerState.state = State.Move;
        SoundManager.Instance.PlayForest();
        gameStarted = true;
    }
    
    public void OpenCredits()
    {
        creditsScreen.SetActive(true);
        PlayerState.state = State.Stand;
    }
    
    public void CloseCredits()
    {
        creditsScreen.SetActive(false);
        if (gameStarted)
        {
            PlayerState.state = State.Move;
        }
    }
    
    public void OpenSettings()
    {
        settingsScreen.SetActive(true);
        settingsExitButton.SetActive(gameStarted);
    }
    
    public void CloseSettings()
    {
        settingsScreen.SetActive(false);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
    
}
