using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class MonsterKillBuff : MonoBehaviour
{
    [SerializeField] private bool grants;
    [SerializeField] private CameraBehavior cam;
    
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (grants)
        {
            if (col.CompareTag("Player"))
            {
                col.AddComponent<MonsterKillBuff>();
                col.GetComponent<MonsterKillBuff>().cam = cam;
                cam.fix = true;
            } 
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.layer == 3 && !col.gameObject.CompareTag("monster"))
        {
            cam.fix = false;
            print(col.gameObject.layer);
            print(col.gameObject.name);
            print(col.gameObject.tag);
            print("Passe ???");
            Destroy(this);
        }
    }
}
