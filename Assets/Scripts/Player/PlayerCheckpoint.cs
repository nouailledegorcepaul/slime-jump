using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCheckpoint : MonoBehaviour
{
    public static Vector3 currentCheckpoint;
    private CameraBehavior cam;
    public CameraChanger cameraChanger;

    private void Start()
    {
        cam = Camera.main.GetComponent<CameraBehavior>();
        currentCheckpoint = new Vector3(-6.93f, -2.44f, 0f);
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            currentCheckpoint = transform.position;
            cam.leftLimitCheckpoint = cameraChanger.leftLimit;
            cam.rightLimitCheckpoint = cameraChanger.rightLimit;
        }
    }
}
