using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class QuestGoalCard : MonoBehaviour
{
    public TextMeshProUGUI text;
    private QuestGoal _goal;
    
    // Update is called once per frame
    void Update()
    {
        if (!_goal.IsUnityNull())
        {
            if (_goal.IsAvailable())
            {
                text.text = _goal.questDescription;
                if (_goal.type == QuestType.Collection)
                {
                    text.text += $" {_goal.currentAmount}/{_goal.goalAmount}";
                }
                if (_goal.IsReached())
                {
                    text.text = $"<s>{_goal.questDescription}</s>";
                    print("QuestGoalCard script destroyed");
                    Destroy(this);
                }
            }
        }
    }

    public void InitCard(QuestGoal g)
    {
        _goal = g;
        if (!_goal.available)
        {
            text.text = "?????????????????";
        }
    }
}
