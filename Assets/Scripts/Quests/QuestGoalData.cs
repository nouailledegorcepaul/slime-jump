using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "QuestGoalData")]
public class QuestGoalData : ScriptableObject
{
    [HideInInspector] public int questId = -1;
    public int id = -1;
    public QuestType type;
    [field: TextArea]
    public string questDescription;
    // [HideInInspector] public int currentAmount;
    [HideInInspector] public int goalAmount;
    [HideInInspector] public int destinationNpcId;
    [HideInInspector] public Convo destinationConvo;
    // [HideInInspector] public bool delivered;
    // [HideInInspector] public bool available;
    [HideInInspector] public List<QuestGoalData> requirements;
    // [HideInInspector] public bool hidden;
}
#if UNITY_EDITOR
[CustomEditor(typeof(QuestGoalData))]
public class QuestGoalDataEditor : Editor
{

    private QuestGoalData data;
    private SerializedProperty goalAmount;
    private SerializedProperty destination;
    private SerializedProperty destinationConvo;
    private SerializedProperty requirements;
    private SerializedProperty hidden;

    private void OnEnable()
    {
        data = target as QuestGoalData;
        goalAmount = serializedObject.FindProperty("goalAmount");
        destination = serializedObject.FindProperty("destinationNpcId");
        destinationConvo = serializedObject.FindProperty("destinationConvo");
        requirements = serializedObject.FindProperty("requirements");
        hidden = serializedObject.FindProperty("hidden");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        serializedObject.Update();
        if (data.type == QuestType.Collection)
        {
            EditorGUILayout.PropertyField(goalAmount);
            EditorGUILayout.PropertyField(hidden);
            EditorGUILayout.PropertyField(requirements, true);
        }

        if (data.type == QuestType.Delivery)
        {
            EditorGUILayout.PropertyField(destination);
            EditorGUILayout.PropertyField(destinationConvo);
            EditorGUILayout.PropertyField(hidden);
            EditorGUILayout.PropertyField(requirements, true);
        }

        if (data.type == QuestType.Final)
        {
            EditorGUILayout.PropertyField(destination);
        }
        
        serializedObject.ApplyModifiedProperties();
    }
}
#endif