using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;


public class QuestGoal
{
    public int questId = -1;
    public int id = -1;
    public QuestType type;
    public string questDescription;
    // public List<GameObject> collectableItems;
    public string collectableItemTag;
    public int currentAmount;
    public int goalAmount;
    public int destinationNpcId;
    public Convo destinationConvo;
    public bool delivered;
    public bool available;
    public bool hidden;
    public List<int> requirements;

    public void Init()
    {
        available = true;
        if (type == QuestType.Collection)
        {
            // SpawnCollectables();
            // InitCollectables();
        }

        if (type == QuestType.Delivery)
        {
            NpcManager.Npcs[destinationNpcId].AddDeliveryQuestGoal(this);
        }
        if (type == QuestType.Final)
        {
            hidden = true;
        }
    }

    public bool IsReached()
    {
        if (type == QuestType.Collection)
        {
            return currentAmount >= goalAmount;
        }

        if (type == QuestType.Delivery)
        {
            return delivered;
        }
        return false;
    }

    public void AddCurrentAmount()
    {
        currentAmount ++;
        UpdateGoal();
    }

    public void Delivered()
    {
        delivered = true;
        UpdateGoal();
    }
    
    public void UpdateGoal()
    {
        if (IsReached())
        {
            QuestUpdater.UpdateQuest(questId);
        }
    }

    public bool IsAvailable()
    {
        if (available)
        {
            return true;
        }
        foreach (int id in requirements)
        {
            if (!QuestInventory.Instance.Quests[questId].goals[id].IsReached())
            {
                return false;
            }
        }
        available = true;
        return true;
    }
}
