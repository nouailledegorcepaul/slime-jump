using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestInventory : MonoBehaviour
{
    public GameObject questTab;
    public Transform questContainer;
    public List<QuestData> allQuestDatas;
    public Dictionary<int, Quest> Quests;
    public static QuestInventory Instance;
    public bool initDone;

    private void Awake()
    {
        if (Instance is null)
        {
            Instance = this;
        }
        else
        {
            print("Already existing instance of QuestManager : GameObject destroyed");
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        Quests = new Dictionary<int, Quest>();
        foreach (QuestData questData in allQuestDatas)
        {
            Quest quest = new Quest
            {
                npcId = questData.npcId,
                id = questData.questId,
                mainQuest = questData.mainQuest,
                lastQuest = questData.lastQuest,
                title = questData.title,
                description = questData.description,
                startQuestConvo = questData.startQuestConvo,
                endQuestConvo = questData.endQuestConvo,
                status = questData.status,
                goals = new List<QuestGoal>(),
                requirements = new List<int>()
            };
            foreach (QuestData req in questData.requirements)
            {
                quest.requirements.Add(req.questId);
            }
            foreach (QuestGoalData goal in questData.goalDatas)
            {
                goal.questId = quest.id;
                QuestGoal g = new QuestGoal
                {
                    questId = quest.id,
                    id = goal.id,
                    type = goal.type,
                    questDescription = goal.questDescription,
                    goalAmount = goal.goalAmount,
                    destinationNpcId = goal.destinationNpcId,
                    destinationConvo = goal.destinationConvo,
                    requirements = new List<int>()
                };
                if (g.type == QuestType.Final)
                {
                    g.hidden = true;
                }
                foreach (QuestGoalData req in goal.requirements)
                {
                    g.requirements.Add(req.id);
                }
                quest.goals.Add(g);
            }
            Quests[quest.id] = quest;
        }
        questTab.SetActive(false);
        foreach (int id in Quests.Keys)
        {
            Quest quest = Quests[id];
            if (quest.status == QuestStatus.Unstarted)
            {
                bool canStart = true;
                foreach (int req in quest.requirements)
                {
                    if (Quests[req].status != QuestStatus.Complete)
                    {
                        canStart = false;
                        break;
                    }
                }
                if (canStart)
                {
                    NpcManager.Npcs[quest.npcId].AddQuestToGive(quest);
                    QuestUpdater.UpdateQuest(quest.id);
                }
            }
        }

        initDone = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerState.state == State.Move)
        {
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                questTab.SetActive(!questTab.activeSelf);
            }  
        }
    }
}