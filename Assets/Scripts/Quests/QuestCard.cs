using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class QuestCard : MonoBehaviour
{
    public Quest quest;
    public TextMeshProUGUI title;

    void Update()
    {
        if (quest.status == QuestStatus.Complete)
        {
            title.text = "<s>" + quest.title + "</s>";
            Destroy(this);
        }
    }

    public void InitCard(Quest quest2)
    {
        quest = quest2;
        title.text = quest.title;
    }


    public void OnClickDetails()
    {
        QuestDetailsWindow.Instance.DisplayQuestDetails(quest);
    }
}
