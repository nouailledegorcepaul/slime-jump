using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestDetailsWindow : MonoBehaviour
{
    public static QuestDetailsWindow Instance;

    public GameObject window;
    public Text Title;
    public Text Description;
    public GameObject questGoalCards;
    // Start is called before the first frame update
    void Awake()
    {
        if (Instance is null)
        {
            Instance = this;
        }
        else
        {
            print("QuestDetailsWindow instance already exists");
            Destroy(gameObject);
        }
    }

    public void DisplayQuestDetails(Quest quest)
    {
        OpenWindow();
        UpdateInfos(quest);
    }
    
    void OpenWindow()
    {
        window.SetActive(true);
    }

    public void CloseWindow()
    {
        window.SetActive(false);
    }
    
    void UpdateInfos(Quest quest)
    {
        Title.text = quest.title;
        Description.text = quest.description;
        foreach (Transform t in questGoalCards.transform)
        {
            Destroy(t.gameObject);
        }
        foreach (QuestGoal goal in quest.goals)
        {
            if (!goal.hidden)
            {
                GameObject go = Instantiate(Resources.Load("Quest Goal Card"), questGoalCards.transform) as GameObject;
                go.GetComponent<QuestGoalCard>().InitCard(goal);  
            }
        }
    }
}
