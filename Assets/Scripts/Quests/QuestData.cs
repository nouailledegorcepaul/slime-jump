using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "QuestData")]
public class QuestData : ScriptableObject
{
    public int npcId = -1;
    public int questId = -1;
    public bool mainQuest;
    public bool lastQuest;
    public string title;
    [field: TextArea] public string description;
    public Convo startQuestConvo, endQuestConvo;
    public QuestStatus status;
    public List<QuestGoalData> goalDatas;
    public List<QuestData> requirements;
}