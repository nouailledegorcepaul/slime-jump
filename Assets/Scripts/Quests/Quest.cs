using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum QuestStatus
{
    Unstarted, Waiting, Started, Pending, Complete
}

public enum QuestType
{
    Delivery, Collection, Final
}

public class Quest
{
    public int npcId = -1;
    public int id = -1;
    public bool mainQuest;
    public bool lastQuest;
    public string title;
    public string description;
    public Convo startQuestConvo, endQuestConvo;
    public QuestStatus status;
    public List<QuestGoal> goals;
    public List<int> requirements;
}