using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class QuestUpdater
{
    public static void UpdateQuest(int id)
    {
        // for each goal in the quest, check if its requirements are filled
        Quest quest = QuestInventory.Instance.Quests[id];
        bool questComplete = true;
        int goals = 0;
        foreach (QuestGoal goal in quest.goals)
        {
            if (goal.type != QuestType.Final)
            {
                goals++;
                if (!goal.available)
                {
                    questComplete = false;
                    bool reqFilled = true;
                    foreach (int goalId in goal.requirements)
                    {
                        if (!quest.goals[goalId].IsReached())
                        {
                            reqFilled = false;
                            break;
                        }
                    }
                    if (reqFilled)
                    {
                        goal.Init();
                    }
                }
                else
                {
                    if (!goal.IsReached())
                    {
                        questComplete = false;
                    }
                }  
            }
        }
        if (quest.status == QuestStatus.Started && questComplete)
        {
            Debug.Log($"Quest {id} complete.");
            QuestGoal goal = quest.goals[^1];
            goal.hidden = false;
            NpcManager.Npcs[goal.destinationNpcId].AddQuestToClaim(quest);
        }
    }

    /*
     * Update quests when one is complete.
     * Allows unstarted quests with requirements to be given by each npc who own them.
     */
    public static void UpdateQuests()
    {
        bool allComplete = true;
        foreach (int id in QuestInventory.Instance.Quests.Keys)
        {
            Quest q = QuestInventory.Instance.Quests[id];
            if (q.status != QuestStatus.Complete)
            {
                allComplete = false;
            }
            if (q.status == QuestStatus.Unstarted)
            {
                bool canStart = true;
                foreach (int idReq in q.requirements)
                {
                    if (QuestInventory.Instance.Quests[idReq].status != QuestStatus.Complete)
                    {
                        canStart = false;
                        break;
                    }
                }
                if (canStart)
                {
                    NpcManager.Npcs[q.npcId].AddQuestToGive(q);
                }
            }
        }

        EndScreen.Instance.complete = allComplete;
        Debug.Log("ALL COMPLETE ? "+ allComplete);
        if (allComplete)
        {
            SoundManager.Instance.PlayEndGameSound();
            EndScreen.Instance.OpenWindow();
        }
    }
}
