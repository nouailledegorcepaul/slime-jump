using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class MonsterKilledBehavior : MonoBehaviour
{
    public float shakeDuration;
    public float shakeAmount, decreaseFactor;
    public Vector3 originalPos;
    public float frequency;
    private float currentFrequency;

    public float fadeFactor;
    // Update is called once per frame


    void Update()
    {
        Color c = GetComponent<SpriteRenderer>().color;
        if (c.a <= 0f)
        {
            GameObject player = GameObject.FindWithTag("Player");
            CameraBehavior cameraBehavior = Camera.main.GetComponent<CameraBehavior>();
            cameraBehavior.fix = false;
            cameraBehavior.leftLimit = new Vector2(86.29f, 2.76f);
            cameraBehavior.rightLimit = new Vector2(93.48f, 6.9f);
            player.transform.position = new Vector3(98, 11, 0);
            SoundManager.Instance.PlayDeepForest();
            Destroy(gameObject);
        }
        c.a -= fadeFactor;
        GetComponent<SpriteRenderer>().color = c;
        
        if (shakeDuration > 0)
        {
            if (currentFrequency > frequency)
            {
                currentFrequency = 0;
                transform.localPosition = new Vector3(originalPos.x + Random.insideUnitSphere.x * shakeAmount , originalPos.y, originalPos.z);

                shakeDuration -= Time.deltaTime * decreaseFactor;
            }
            else
            {
                currentFrequency += Time.deltaTime;
            }
        }
        else
        {
            shakeDuration = 0f;
            transform.localPosition = originalPos;
        }
    }
}
