using UnityEngine;

public enum DialogueType
{
    Greetings, Base, Quest
}

[CreateAssetMenu(menuName = "Convo")]
public class Convo : ScriptableObject
{
    public int id = -1;
    public DialogueType type;
    [field: TextArea]
    public string[] sentences;
}