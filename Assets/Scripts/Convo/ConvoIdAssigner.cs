using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ConvoIdAssigner
{
    public static int maxId;

    public static void AssignId(Convo convo)
    {
        if (convo.id == -1)
        {
            convo.id = maxId;
            maxId++;
        }
    }
}
